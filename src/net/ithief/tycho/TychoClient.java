package net.ithief.tycho;

public class TychoClient {

	private String username;
	private String displayName;
	private TychoServer server;

	public TychoClient(TychoServer server, String username, String displyName) {
		this.server = server;
		this.username = username;
		this.displayName = displyName;
	}

	public String getDisplayName() {
		return displayName;
	}

	protected void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * For subclasses
	 */
	protected TychoClient() {

	}
	
	protected void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public boolean isLegit() {
		return true;
	}

	public void handleFriendRequestDelivered(TychoClient client) {
		server.getDatabase().createFriendRequest(client.getUsername(), username);
	}

	public void handleFriendRequestAccepted(TychoClient client) {
		server.getDatabase().acceptFriendRequest(username, client.getUsername());
	}

	public void handleFriendRequestRejected(TychoClient client) {
		server.getDatabase().rejectFriendRequest(username, client.getUsername());
	}
}
