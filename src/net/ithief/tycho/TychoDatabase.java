package net.ithief.tycho;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TychoDatabase {

	private final Object syncer = new Object();
	private Connection conn;
	private TychoServer server;

	public TychoDatabase(TychoServer server, String databasePrefix) {
		this.server = server;
		String url = "jdbc:sqlite:" + databasePrefix + "dino.db";
		try {
			conn = DriverManager.getConnection(url);
			if (conn != null) {
				conn.getMetaData();
				execute("CREATE TABLE IF NOT EXISTS users (\n"
						+ "	id integer PRIMARY KEY,\n"
						+ "	username text NOT NULL,\n"
						+ "	displayname text NOT NULL,\n"
						+ "	password text NOT NULL,\n"
						+ "	lastip text NOT NULL,\n"
						+ "	rank integer NOT NULL\n"
						+ ");");
				execute("CREATE TABLE IF NOT EXISTS friendships (\n"
						+ "	id integer PRIMARY KEY,\n"
						+ "	name1 text NOT NULL,\n"
						+ "	name2 text NOT NULL,\n"
						+ "	name1_accepted integer NOT NULL,\n"
						+ "	name2_accepted integer NOT NULL\n"
						+ ");");
			}
		} catch (Exception e) {
			e.printStackTrace();
			errorClose();
		}
	}

	private void errorClose() {
		server.close();
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void execute(String sql) {
		synchronized (syncer) {
			Statement stmt;
			try {
				stmt = conn.createStatement();
				stmt.execute(sql);
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				errorClose();
			}
		}
	}

	private void execute(String sql, Object... data) {
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			for (int i = 0; i < data.length; i++) {
				Object d = data[i];
				if (d instanceof String) {
					stmt.setString(i + 1, (String) d);
				} else if (d instanceof Integer) {
					stmt.setInt(i + 1, (int) d);
				} else {
					throw new Exception("Unknown query type: " + d.getClass());
				}
			}
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			errorClose();
		}
	}

	private ResultSet select(String sql, Object... data) {
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			for (int i = 0; i < data.length; i++) {
				Object d = data[i];
				if (d instanceof String) {
					stmt.setString(i + 1, (String) d);
				} else if (d instanceof Integer) {
					stmt.setInt(i + 1, (int) d);
				} else {
					throw new Exception("Unknown query type: " + d.getClass());
				}
			}
			ResultSet rs = stmt.executeQuery();
			return rs;
		} catch (Exception e) {
			e.printStackTrace();
			errorClose();
		}
		return null;
	}
	

	public List<TychoClient> getSentFriendRequests(TychoOnlineClient client) {
		MyResult[] mrs = new MyResult[2];
		List<TychoClient> friends = new ArrayList<>();
		synchronized (syncer) {
			ResultSet rs1 = select("SELECT users.id as friendID, users.username as friendUsername, users.displayname as friendDisplayName, users.rank as friendRank FROM users, friendships WHERE friendships.name1 = ? AND friendships.name2 = users.username AND name1_accepted = 1 AND name2_accepted = 0", client.getUsername());
			ResultSet rs2 = select("SELECT users.id as friendID, users.username as friendUsername, users.displayname as friendDisplayName, users.rank as friendRank FROM users, friendships WHERE friendships.name2 = ? AND friendships.name1 = users.username AND name1_accepted = 0 AND name2_accepted = 1", client.getUsername());
			if (rs1 == null || rs2 == null) {
				errorClose();
				return null;
			}
			mrs[0] = new MyResult(rs1);
			mrs[1] = new MyResult(rs2);
			for (MyResult mr : mrs) {
				for (MyRow row : mr.getRows()) {
					String username = row.getString("friendUsername");
					String displayname = row.getString("displayname");
					TychoOnlineClient tc = server.getOnlineClient(username);
					if (tc == null) { // Offline client
						friends.add(new TychoClient(server, username, displayname));
					} else { // Online client
						friends.add(tc);
					}
				}
			}
			return friends;
		}
	}
	
	public List<TychoClient> getReceivedFriendRequests(TychoOnlineClient client) {
		MyResult[] mrs = new MyResult[2];
		List<TychoClient> friends = new ArrayList<>();
		synchronized (syncer) {
			ResultSet rs1 = select("SELECT users.id as friendID, users.username as friendUsername, users.displayname as friendDisplayName, users.rank as friendRank FROM users, friendships WHERE friendships.name1 = ? AND friendships.name2 = users.username AND name1_accepted = 0 AND name2_accepted = 1", client.getUsername());
			ResultSet rs2 = select("SELECT users.id as friendID, users.username as friendUsername, users.displayname as friendDisplayName, users.rank as friendRank FROM users, friendships WHERE friendships.name2 = ? AND friendships.name1 = users.username AND name1_accepted = 1 AND name2_accepted = 0", client.getUsername());
			if (rs1 == null || rs2 == null) {
				errorClose();
				return null;
			}
			mrs[0] = new MyResult(rs1);
			mrs[1] = new MyResult(rs2);
			for (MyResult mr : mrs) {
				for (MyRow row : mr.getRows()) {
					String username = row.getString("friendUsername");
					String displayname = row.getString("displayname");
					TychoOnlineClient tc = server.getOnlineClient(username);
					if (tc == null) { // Offline client
						friends.add(new TychoClient(server, username, displayname));
					} else { // Online client
						friends.add(tc);
					}
				}
			}
			return friends;
		}
	}

	public List<TychoClient> getClientFriends(TychoOnlineClient client) {
		MyResult[] mrs = new MyResult[2];
		List<TychoClient> friends = new ArrayList<>();
		synchronized (syncer) {
			ResultSet rs1 = select("SELECT users.id as friendID, users.username as friendUsername, users.displayname as friendDisplayName, users.rank as friendRank FROM users, friendships WHERE friendships.name1 = ? AND friendships.name2 = users.username AND name1_accepted = 1 AND name2_accepted = 1", client.getUsername());
			ResultSet rs2 = select("SELECT users.id as friendID, users.username as friendUsername, users.displayname as friendDisplayName, users.rank as friendRank FROM users, friendships WHERE friendships.name2 = ? AND friendships.name1 = users.username AND name1_accepted = 1 AND name2_accepted = 1", client.getUsername());
			if (rs1 == null || rs2 == null) {
				errorClose();
				return null;
			}
			mrs[0] = new MyResult(rs1);
			mrs[1] = new MyResult(rs2);
			for (MyResult mr : mrs) {
				for (MyRow row : mr.getRows()) {
					String username = row.getString("friendUsername");
					String displayname = row.getString("displayname");
					TychoOnlineClient tc = server.getOnlineClient(username);
					if (tc == null) { // Offline client
						friends.add(new TychoClient(server, username, displayname));
					} else { // Online client
						friends.add(tc);
					}
				}
			}
			return friends;
		}
	}

	public boolean login(String username, String password, String ip) {
		synchronized (syncer) {
			ResultSet rs = select("select * from users WHERE username = ? and password = ?", username, password);
			if (rs == null) {
				errorClose();
				return false;
			}
			MyResult mr = new MyResult(rs);
			int cnt = mr.getSize();
			if (cnt == 0) { // Register
				closeRS(rs);
				execute("INSERT INTO users (username,password,displayname,lastip,rank) VALUES (?, ?, ?, ?, 0)", username, password, "", ip);
				return true;
			} else if (cnt == 1) { // Login
				closeRS(rs);
				return true;
			} else { // Error ?
				closeRS(rs);
				return false;
			}
		}
	}

	public TychoClient getClient(String username) {
		synchronized (syncer) {
			ResultSet rs = select("select * from users WHERE username = ?", username);
			if (rs == null) {
				errorClose();
				return null;
			}
			MyResult mr = new MyResult(rs);
			int cnt = mr.getSize();
			if (cnt == 1) {
				String displayname = mr.getRow(0).getString("displayname");
				closeRS(rs);
				return new TychoClient(server, username, displayname);
			}
			closeRS(rs);
			return null;
		}
	}

	private void closeRS(ResultSet rs) {
		try {
			rs.getStatement().close();
		} catch (SQLException e1) {
		}
		try {
			rs.close();
		} catch (SQLException e) {
		}
	}

	private static class MyResult {

		private int size;
		private List<MyRow> rows = new ArrayList<>();

		public MyResult(ResultSet rs) {
			try {
				while (rs.next()) {
					int cols = rs.getMetaData().getColumnCount();
					Map<String, Object> data = new HashMap<>();
					for (int i = 0; i < cols; i++) {
						data.put(rs.getMetaData().getColumnName(i + 1), rs.getObject(i + 1));
					}
					rows.add(new MyRow(data));
				}
				size = rows.size();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		public List<MyRow> getRows() {
			return rows;
		}

		public int getSize() {
			return size;
		}

		public MyRow getRow(int index) {
			return rows.get(index);
		}

	}

	private static class MyRow {
		private Map<String, Object> data;

		private MyRow(Map<String, Object> map) {
			this.data = map;
		}

		public String getString(String columnName) {
			return (String) data.get(columnName);
		}

		public int getInt(String columnName) {
			return (int) data.get(columnName);
		}
	}

	public void createFriendRequest(String username, String username2) {
		synchronized (this) {
			ResultSet rs = select("SELECT * FROM friendships WHERE (name1 = ? AND name2 = ?) OR (name1 = ? AND name2 = ?) ", username, username2, username, username2);
			if (rs == null) { // Error?
				errorClose();
				return;
			}
			MyResult mr = new MyResult(rs);
			int size = mr.getSize();
			if (size == 0) { // No such friendship
				execute("INSERT INTO friendships (name1, name2, name1_accepted, name2_accepted) VALUES (?, ?, ?, ?)", username, username2, 1, 0);
			} else if (size == 1) { // Already a friendship, update
				boolean numbersSet = mr.getRow(0).getString("name1").equals(username);
				if (numbersSet) {
					execute("UPDATE friendships SET name1_accepted = ? WHERE name1 = ? and name2 = ?", 1, username, username2);
				} else {
					execute("UPDATE friendships SET name2_accepted = ? WHERE name2 = ? and name1 = ?", 1, username, username2);
				}
			} else { // Huh?
				errorClose();
			}
		}
	}

	public void acceptFriendRequest(String acceptor, String friend) {
		synchronized (this) {
			execute("UPDATE friendships SET name1_accepted = ? WHERE name1 = ? and name2 = ?", 1, acceptor, friend);
			execute("UPDATE friendships SET name2_accepted = ? WHERE name2 = ? and name1 = ?", 1, acceptor, friend);
		}
	}

	public void rejectFriendRequest(String rejector, String friend) {
		synchronized (this) {
			execute("UPDATE friendships SET name1_accepted = ? WHERE name1 = ? and name2 = ?", 0, rejector, friend);
			execute("UPDATE friendships SET name2_accepted = ? WHERE name2 = ? and name1 = ?", 0, rejector, friend);
		}
	}

}
