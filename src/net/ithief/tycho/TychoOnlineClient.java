package net.ithief.tycho;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TychoOnlineClient extends TychoClient {

	private TychoSocketClient socketClient;

	private boolean logged = false;

	private TychoServer server;

	private final String IP;

	private final Map<String, TychoClient> friends = new HashMap<>();

	public void friendStatusChange(TychoClient friend, STATUS newStatus) {
		if (friend == this) { // What?
			System.err.println("This should never display");
			return;
		}
		String username = friend.getUsername();
		synchronized (this) {
			friends.put(username, friend);
			try {
				socketClient.writeInt(0); // Not a response
				socketClient.writeInt(MESSAGES.USER_STATUS_CHANGED.getCode());
				socketClient.writeInt(newStatus.getCode());
				socketClient.writeString(friend.getUsername());
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
		}
	}

	protected void register() {
		server.registerClient(this);
		alertFriends(STATUS.ONLINE);
	}

	protected void unregister() {
		server.unregisterClient(this);
		alertFriends(STATUS.OFFLINE);
	}

	public TychoOnlineClient(TychoServer server, TychoSocketClient tychoSocketClient) {
		super(server, null, null);
		synchronized (this) {
			this.socketClient = tychoSocketClient;
			this.server = server;
			this.IP = tychoSocketClient.getIP();
			if (readLogin()) {
				logged = true;
				register();
				sendLoginConfirmation();
				sendFriends();
				_thread.start();
			}
		}
	}

	private void sendFriends() {
		synchronized (this) {
			this.friends.clear();
			List<TychoClient> friends = server.getDatabase().getClientFriends(this);
			List<TychoClient> receivedFriendRequests = server.getDatabase().getReceivedFriendRequests(this);
			List<TychoClient> sentFriendRequests = server.getDatabase().getSentFriendRequests(this);
			for (TychoClient friend : friends) {
				this.friends.put(friend.getUsername(), friend);
				if (friend instanceof TychoOnlineClient) {
					friendStatusChange(friend, STATUS.ONLINE);
				} else {
					friendStatusChange(friend, STATUS.OFFLINE);
				}
			}
			for (TychoClient wannaBeFriend : receivedFriendRequests) {
				try {
					socketClient.writeInt(0); // Not a response
					socketClient.writeInt(MESSAGES.RECEIVED_FRIEND_REQUEST.getCode());
					socketClient.writeInt(0); // Not a response
					socketClient.writeString(wannaBeFriend.getUsername());
				} catch (IOException e) {
					close();
					e.printStackTrace();
				}
			}
			for (TychoClient potentialFriend : sentFriendRequests) {
				try {
					socketClient.writeInt(0); // Not a response
					socketClient.writeInt(MESSAGES.SEND_FRIEND_REQUEST.getCode());
					socketClient.writeInt(0); // Not a response
					socketClient.writeString(potentialFriend.getUsername());
				} catch (IOException e) {
					close();
					e.printStackTrace();
				}
			}

		}
	}

	private void alertFriends(STATUS newStatus) {
		synchronized (this) {
			for (TychoClient friend : friends.values()) {
				if (friend instanceof TychoOnlineClient) {
					TychoOnlineClient onlineFriend = (TychoOnlineClient) friend;
					synchronized (onlineFriend) {
						onlineFriend.friendStatusChange(this, newStatus);
					}
				}
			}
		}
	}

	private void sendLoginConfirmation() {
		synchronized (this) {
			try {
				socketClient.writeInt(0);
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
		}
	}

	private boolean readLogin() {
		try {
			int messageID = socketClient.readInt();
			String username = socketClient.readString();
			String password = socketClient.readString();
			super.setUsername(username);
			socketClient.writeInt(messageID);
			return server.login(username, password, IP);
		} catch (IOException e) {
			e.printStackTrace();
			close();
			return false;
		}
	}

	private boolean closing = false;

	public void close() {
		synchronized (this) {
			if (!closing) {
				closing = true;
				logged = false;
				socketClient.close();
				unregister();
			}
		}
	}

	@Override
	public boolean isLegit() {
		return logged;
	}

	private void handleLoop() {
		try {
			MESSAGES msg = MESSAGES.get(socketClient.readInt());
			int messageID = socketClient.readInt();
			synchronized (this) {
				switch (msg) {
				case UNKNOWN:
				default:
					close();
					break;
				case SEND_MESSAGE:
					handleSendMessage(messageID);
					break;
				case SEND_GROUP_MESSAGE:
					handleSendGroupMessage(messageID);
					break;
				case SEND_FRIEND_REQUEST:
					handleSendFriendRequest(messageID);
					break;
				case ACCEPT_FRIEND_REQUEST:
					handleAcceptFriendRequest(messageID);
					break;
				case REJECT_FRIEND_REQUEST:
					handleRejectFriendRequest(messageID);
					break;
				case CREATE_GROUP:
					handleGroupCreate(messageID);
					break;
				case DISPOSE_GROUP:
					handleGroupDispose(messageID);
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	private void handleGroupDispose(int messageID) {
		// TODO Auto-generated method stub

	}

	private void handleGroupCreate(int messageID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleFriendRequestRejected(TychoClient client) {
		synchronized (this) {
			if (friends.containsKey(client.getUsername())) {
				super.handleFriendRequestRejected(client);
				friends.remove(client.getUsername());
				try {
					socketClient.writeInt(0);
					socketClient.writeInt(MESSAGES.REJECT_FRIEND_REQUEST.getCode());
					socketClient.writeString(client.getUsername());
				} catch (IOException e) {
					e.printStackTrace();
					close();
				}
			}
		}
	}

	private void handleRejectFriendRequest(int messageID) {
		try {
			String username = socketClient.readString();
			socketClient.writeInt(messageID);
			TychoClient client = friends.get(username);
			if (client != null) { // Already a friend -> remove
				socketClient.writeInt(0);
				client.handleFriendRequestRejected(this);
			} else { // Not our friend, does he even exist?
				TychoClient friend = server.getClient(username);
				if (friend == null) {// no such user
					socketClient.writeInt(1);
				} else {
					friend.handleFriendRequestRejected(this);
					socketClient.writeInt(0); // Done
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	@Override
	public void handleFriendRequestAccepted(TychoClient client) {
		synchronized (this) {
			if (!friends.containsKey(client.getUsername())) {
				super.handleFriendRequestAccepted(client);
				friends.put(client.getUsername(), client);
				try {
					socketClient.writeInt(0);
					socketClient.writeInt(MESSAGES.ACCEPT_FRIEND_REQUEST.getCode());
					socketClient.writeString(client.getUsername());
				} catch (IOException e) {
					e.printStackTrace();
					close();
				}
			}
		}
	}

	private void handleAcceptFriendRequest(int messageID) {
		try {
			String username = socketClient.readString();
			socketClient.writeInt(messageID);
			TychoClient client = friends.get(username);
			if (client != null) { // Already a friend
				socketClient.writeInt(0);
			} else { // Not our friend, does he even exist?
				TychoClient friend = server.getClient(username);
				if (friend == null) {// no such user
					socketClient.writeInt(1);
				} else {
					friend.handleFriendRequestAccepted(this);
					socketClient.writeInt(2); // Accepted
					super.handleFriendRequestAccepted(friend);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	@Override
	public void handleFriendRequestDelivered(TychoClient client) {
		synchronized (this) {
			if (!friends.containsKey(client.getUsername())) {
				super.handleFriendRequestDelivered(client);
				try {
					socketClient.writeInt(0);
					socketClient.writeInt(MESSAGES.SEND_FRIEND_REQUEST.getCode());
					socketClient.writeString(client.getUsername());
				} catch (IOException e) {
					e.printStackTrace();
					close();
				}
			}
		}
	}

	private void handleSendFriendRequest(int messageID) {
		try {
			String username = socketClient.readString();
			socketClient.writeInt(messageID);
			TychoClient client = friends.get(username);
			if (client != null) { // Already a friend
				socketClient.writeInt(0);
			} else { // Not our friend, does he even exist?
				TychoClient friend = server.getClient(username);
				if (friend == null) {// no such user
					socketClient.writeInt(1);
				} else {
					friend.handleFriendRequestDelivered(this);
					socketClient.writeInt(2); // Delivered
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	private void handleSendGroupMessage(int messageID) {
		// TODO Auto-generated method stub

	}

	private void handleSendMessage(int messageID) {
		try {
			String receiver = socketClient.readString();
			String message = socketClient.readString();
			TychoClient client = friends.get(receiver);
			socketClient.writeInt(messageID);
			if (client == null) { // No such friend
				socketClient.writeInt(0);
			} else {
				if (client instanceof TychoOnlineClient) { // Our friend is online
					TychoOnlineClient onlineClient = (TychoOnlineClient) client;
					synchronized (onlineClient) {
						try {
							onlineClient.socketClient.writeInt(0);
							onlineClient.socketClient.writeInt(MESSAGES.SEND_MESSAGE.getCode());
							onlineClient.socketClient.writeString(getUsername());
							onlineClient.socketClient.writeString(message);
						} catch (IOException ee) {
							ee.printStackTrace();
							onlineClient.close();
							socketClient.writeInt(3); // Message not delivered
							return;
						}
						socketClient.writeInt(2); // Message delivered
					}

				} else { // Our friend is not online
					socketClient.writeInt(1);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	private Thread _thread = new Thread() {
		@Override
		public void run() {
			while (logged) {
				handleLoop();
			}
			close();
		}

	};

	
	enum STATUS {
		ONLINE(0), OFFLINE(1);
		private final int opCode;

		private STATUS(int opCode) {
			this.opCode = opCode;
		}

		public int getCode() {
			return opCode;
		}
	}

	enum MESSAGES {
		SEND_MESSAGE(0), SEND_GROUP_MESSAGE(1), SEND_FRIEND_REQUEST(2), RECEIVED_FRIEND_REQUEST(3), ACCEPT_FRIEND_REQUEST(4), REJECT_FRIEND_REQUEST(5), CREATE_GROUP(6), DISPOSE_GROUP(7), GET_GROUP(8), UNKNOWN(9), USER_STATUS_CHANGED(10);

		private final int opCode;
		private static final int size = MESSAGES.values().length;

		MESSAGES(int opCode) {
			this.opCode = opCode;
		}

		public int getCode() {
			return opCode;
		}

		public static MESSAGES get(int code) {
			if (code >= 0 && code < size) {
				return MESSAGES.values()[code];
			} else {
				return UNKNOWN;
			}
		}
	}
}
