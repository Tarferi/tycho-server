package net.ithief.tycho;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;

public class TychoServer {

	private ServerSocket socket;

	//private List<TychoThreadPool> threadPool = new ArrayList<>();

	private TychoDatabase database = new TychoDatabase(this, "Tycho_");

	private boolean running = true;

	public TychoDatabase getDatabase() {
		return database;
	}

	private Map<String, TychoOnlineClient> onlineClients = new HashMap<>();

	public TychoClient getClient(String username) {
		synchronized (onlineClients) {
			TychoClient client = onlineClients.get(username);
			if (client != null) {
				return client;
			} else {
				return database.getClient(username);
			}
		}
	}

	public TychoOnlineClient getOnlineClient(String username) {
		synchronized (onlineClients) {
			return onlineClients.get(username);
		}
	}

	public TychoServer(int port, int threadCount, int threadSize) {
		try {
			socket = new ServerSocket(port);
			/*
			for (int i = 0; i < threadCount; i++) {
				threadPool.add(new TychoThreadPool(threadSize));
			}
			*/
			mainLoop();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	private TychoThreadPool findAvailablePool() {
		return threadPool.get(0);
	}
	*/

	private void mainLoop() {
		while (running) {
			try {
				new TychoSocketClient(this, socket.accept());
				//findAvailablePool().addClient(newClient);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void close() {
		running = false;
		try {
			socket.close();
		} catch (Throwable e) {
		}
	}

	public void registerClient(TychoOnlineClient tychoClient) {
		synchronized (onlineClients) {
			onlineClients.put(tychoClient.getUsername(), tychoClient);
		}
	}

	public void unregisterClient(TychoOnlineClient tychoClient) {
		synchronized (onlineClients) {
			onlineClients.remove(tychoClient.getUsername());
		}
	}

	public boolean login(String username, String password, String ip) {
		synchronized (onlineClients) {
			if (onlineClients.containsKey(username)) {
				return false;
			}
		}
		return database.login(username, password, ip);
	}
}
