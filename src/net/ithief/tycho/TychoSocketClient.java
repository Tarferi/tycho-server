package net.ithief.tycho;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TychoSocketClient {

	private Socket socket;

	private InputStream in;
	private OutputStream out;

	public TychoSocketClient(TychoServer server, Socket socket) {
		this.socket = socket;
		try {
			this.in = socket.getInputStream();
			this.out = socket.getOutputStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
		new TychoOnlineClient(server, this);
	}

	private int readByte() throws IOException {
		int i = in.read();
		if (i >= 0) {
			return i;
		} else {
			throw new IOException("Socket closed?");
		}
	}

	private int readShort() throws IOException {
		return (readByte() << 8) | readByte();
	}

	protected int readInt() throws IOException {
		return (readShort() << 16) | readShort();
	}

	private byte[] readArray(int length) throws IOException {
		byte[] buffer = new byte[length];
		in.read(buffer, 0, length);
		return buffer;
	}

	protected String readString() throws IOException {
		return new String(readArray(readInt()));
	}

	private void writeByte(int data) throws IOException {
		out.write(data & 0xff);
	}

	private void writeShort(int data) throws IOException {
		writeByte(data >> 8);
		writeByte(data);
	}

	protected void writeInt(int data) throws IOException {
		writeShort(data >> 16);
		writeShort(data);
	}

	private void writeArray(byte[] data) throws IOException {
		out.write(data, 0, data.length);
	}

	protected void writeString(String str) throws IOException {
		writeInt(str.length());
		writeArray(str.getBytes());
	}

	public void close() {
		try {
			in.close();
		} catch (Throwable e) {
		}
		try {
			out.close();
		} catch (Throwable e) {
		}
		try {
			socket.close();
		} catch (Throwable e) {
		}
	}

	public String getIP() {
		return socket.getInetAddress().toString();
	}
}
