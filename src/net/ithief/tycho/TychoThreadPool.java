package net.ithief.tycho;

import java.util.ArrayList;
import java.util.List;

public class TychoThreadPool {

	private int clientCount = 0;
	private int maxClientCount = 0;

	private List<TychoSocketClient> clients = new ArrayList<>();

	public TychoThreadPool(int threadSize) {
		maxClientCount = threadSize;
		thread.start();
	}

	public void mainThreadLoop() {
		while (true) {
			synchronized (clients) {
				for (TychoSocketClient client : clients) {
				}
			}
		}
	}

	public void addClient(TychoSocketClient client) {
		synchronized (clients) {
			if (!clients.contains(client)) {
				clients.add(client);
				clientCount++;
			}
		}
	}

	public void removeClient(TychoSocketClient client) {
		synchronized (clients) {
			if (clients.contains(client)) {
				clients.remove(client);
				clientCount--;
			}
		}
	}

	public boolean isAvailable() {
		return clientCount < maxClientCount;
	}

	private Thread thread = new Thread() {
		@Override
		public void run() {
			mainThreadLoop();
		}
	};
}
